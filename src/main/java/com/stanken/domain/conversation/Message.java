package com.stanken.domain.conversation;

import java.util.UUID;

/**
 * Created by erik on 2017-06-19.
 */
public class Message {
    private final UUID id;
    private final UUID conversationID;
    private final UserID sender;
    private final String message;
    
    public Message(UUID id, UUID conversationID, String message, UserID sender) {
        this.id = id;
        this.conversationID = conversationID;
        this.message = message;
        this.sender = sender;
    }
}
