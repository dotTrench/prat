package com.stanken.application.conversation.exceptions;

import com.stanken.application.shared.CommandException;

import java.util.UUID;

/**
 * Created by erik on 2017-06-19.
 */
public class ConversationNotActiveException extends CommandException {
    public ConversationNotActiveException(UUID conversationID) {
        super(String.format("Conversation %s is expired", conversationID));
    }
}
