package com.stanken.application.conversation;

import com.stanken.application.conversation.commands.SendMessageCommand;
import com.stanken.application.conversation.exceptions.ConversationNotActiveException;
import com.stanken.application.conversation.exceptions.ConversationNotFoundException;
import com.stanken.application.conversation.exceptions.UserNotPartOfConversationException;
import com.stanken.domain.conversation.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Created by erik on 2017-06-19.
 */
class MessageServiceTest {
    private MessageService messageService;
    @Mock
    private MessageRepository messageRepository;
    @Mock
    private ConversationRepository conversationRepository;

    private Conversation conversation;

    private UserID user;

    @BeforeEach
    void setUp() {
        initMocks(this);

        user = new UserID();

        conversation = Conversation.start(Arrays.asList(user, new UserID(), new UserID()));

        when(conversationRepository.findByID(conversation.getID()))
                .thenReturn(Optional.of(conversation));

        messageService = new MessageService(conversationRepository, messageRepository);
    }

    @Test
    void sendMessage() {
        messageService.sendMessage(new SendMessageCommand(conversation.getID(), "Hello world", user.asUUID()));

        verify(messageRepository).save(any(Message.class));
    }

    @Test
    void userCanNotSendMessageIfNotPartOfConversation() {
        assertThrows(UserNotPartOfConversationException.class,
                () -> messageService.sendMessage(new SendMessageCommand(conversation.getID(), "Some Message", UUID.randomUUID())));

    }

    @Test
    void userCanNotSendMessageIfUserHasLeftConversation() {
        conversation.leave(user);

        assertThrows(UserNotPartOfConversationException.class,
                () -> messageService.sendMessage(new SendMessageCommand(conversation.getID(), "Some", user.asUUID())));
    }

    @Test
    void throwsIfConversationIsNotFound() {
        when(conversationRepository.findByID(conversation.getID()))
                .thenReturn(Optional.empty());

        assertThrows(ConversationNotFoundException.class,
                () -> messageService.sendMessage(new SendMessageCommand(conversation.getID(), "Hello", user.asUUID())));
    }

    @Test
    void throwsIfConversationIsExpired() {
        conversation.expire();

        assertThrows(ConversationNotActiveException.class,
                () -> messageService.sendMessage(new SendMessageCommand(conversation.getID(), "Hell", user.asUUID())));
    }
}