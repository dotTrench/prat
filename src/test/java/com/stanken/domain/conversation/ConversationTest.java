package com.stanken.domain.conversation;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by erik on 2017-06-15.
 */
class ConversationTest {
    @Test
    void cantStartConversationWithLessThan2Participants() {
        assertThrows(IllegalArgumentException.class, () ->
                Conversation.start(Collections.singletonList(new UserID())));
    }

    @Test
    void leavingConversationMakesUserNoLongerPartOfConversation() {
        UserID userID = new UserID();
        Conversation conversation = Conversation.start(Arrays.asList(userID, new UserID()));
        conversation.leave(userID);

        assertFalse(conversation.isUserParticipant(userID));
    }

    @Test
    void startingNewConversationMakesUserPartOfConversation() {
        UserID userID = new UserID();

        Conversation conversation = Conversation.start(Arrays.asList(userID, new UserID()));

        assertTrue(conversation.isUserParticipant(userID));
    }

    @Test
    void leavingConversationThatUserIsNotPartOfThrowsException() {
        Conversation conversation = Conversation.start(Arrays.asList(new UserID(), new UserID(), new UserID()));

        assertThrows(IllegalArgumentException.class,  () -> conversation.leave(new UserID()));
    }

    @Test
    void leavingConversationWithOnly2ActiveParticipantsStopsTheConversation() {
        UserID userID = new UserID();
        Conversation conversation = Conversation.start(Arrays.asList(userID, new UserID()));

        conversation.leave(userID);

        assertFalse(conversation.isActive());
    }

    @Test
    void expireMakesTheConversationInactive() {
        Conversation conversation = Conversation.start(Arrays.asList(new UserID(), new UserID()));

        conversation.expire();

        assertFalse(conversation.isActive());
    }
}