package com.stanken.application.conversation.commands;

import java.util.UUID;

/**
 * Created by erik on 2017-06-19.
 */
public class LeaveConversationCommand {
    public final UUID conversationID;
    public final UUID userID;

    public LeaveConversationCommand(UUID conversationID, UUID userID) {
        this.conversationID = conversationID;
        this.userID = userID;
    }
}
