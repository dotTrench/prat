package com.stanken.application.conversation;

import com.stanken.application.conversation.commands.LeaveConversationCommand;
import com.stanken.application.conversation.commands.StartConversationCommand;
import com.stanken.application.conversation.exceptions.NotEnoughParticipantsException;
import com.stanken.application.conversation.exceptions.UserNotPartOfConversationException;
import com.stanken.domain.conversation.Conversation;
import com.stanken.domain.conversation.ConversationRepository;
import com.stanken.domain.conversation.Participant;
import com.stanken.domain.conversation.UserID;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Created by erik on 2017-06-15.
 */
class ConversationServiceTest {
    private ConversationService conversationService;
    @Mock
    private ConversationRepository conversationRepository;

    @BeforeEach
    void setUp() {
        initMocks(this);
        conversationService = new ConversationService(conversationRepository);
    }

    @Test
    void startConversation() throws Exception {
        Collection<UUID> participants = Arrays.asList(UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID());
        conversationService.startConversation(new StartConversationCommand(participants));
        verify(conversationRepository).save(any(Conversation.class));
    }

    @Test
    void cantStartConversationWithLessThan2Participants() throws Exception {
        Collection<UUID> participants = Arrays.asList(UUID.randomUUID());

        Assertions.assertThrows(NotEnoughParticipantsException.class, () -> conversationService.startConversation(new StartConversationCommand(participants)));

        verify(conversationRepository, never()).save(any(Conversation.class));
    }

    @Test
    void startConversationSavesAnActiveConversation() throws Exception {
        Collection<UUID> participants = Arrays.asList(UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID());
        conversationService.startConversation(new StartConversationCommand(participants));
        ArgumentCaptor<Conversation> argumentCaptor = ArgumentCaptor.forClass(Conversation.class);


        verify(conversationRepository).save(argumentCaptor.capture());

        assertTrue(argumentCaptor.getValue().isActive());
    }

    @Test
    void leaveConversation() {
        UserID userID = new UserID();
        Conversation conversation = Conversation.start(Arrays.asList(userID, new UserID()));
        when(conversationRepository.findByID(any(UUID.class)))
                .thenReturn(Optional.of(conversation));

        conversationService.leaveConversation(new LeaveConversationCommand(conversation.getID(), userID.asUUID()));
        ArgumentCaptor<Conversation> captor = ArgumentCaptor.forClass(Conversation.class);

        verify(conversationRepository).save(captor.capture());

        assertFalse(captor.getValue().isUserParticipant(userID));
    }

    @Test
    void leavingConversationThatUserIsNotPartOfThrowsException() {
        Conversation conversation = Conversation.start(Arrays.asList(new UserID(), new UserID(), new UserID()));

        when(conversationRepository.findByID(any(UUID.class)))
                .thenReturn(Optional.of(conversation));

        assertThrows(UserNotPartOfConversationException.class, () -> conversationService.leaveConversation(new LeaveConversationCommand(UUID.randomUUID(), UUID.randomUUID())));
    }


}