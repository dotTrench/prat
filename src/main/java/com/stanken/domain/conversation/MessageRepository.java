package com.stanken.domain.conversation;

/**
 * Created by erik on 2017-06-19.
 */
public interface MessageRepository {
    void save(Message message);
}
