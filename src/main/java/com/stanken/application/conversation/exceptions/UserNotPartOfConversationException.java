package com.stanken.application.conversation.exceptions;

import com.stanken.application.shared.CommandException;

import java.util.UUID;

/**
 * Created by erik on 2017-06-19.
 */
public class UserNotPartOfConversationException extends CommandException {
    public UserNotPartOfConversationException(UUID conversationID, UUID userID) {
        super(String.format("User %s is not part of conversation %s", userID, conversationID));

    }
}
