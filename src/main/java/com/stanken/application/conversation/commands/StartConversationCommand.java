package com.stanken.application.conversation.commands;

import java.util.Collection;
import java.util.UUID;

/**
 * Created by erik on 2017-06-15.
 */
public class StartConversationCommand {
    public final Collection<UUID> participants;

    public StartConversationCommand(Collection<UUID> participants) {
        this.participants = participants;
    }
}
