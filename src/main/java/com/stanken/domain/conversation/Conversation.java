package com.stanken.domain.conversation;

import java.util.Collection;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Created by erik on 2017-06-15.
 */
public class Conversation {
    private final Collection<Participant> participants;
    private final UUID id;
    private boolean active;

    public Conversation(UUID id, Collection<Participant> participants, boolean active) {
        if (participants.size() < 2) throw new IllegalArgumentException("a conversation needs at least 2 participants");
        this.active = active;
        this.id = id;
        this.participants = participants;
    }

    public static Conversation start(Collection<UserID> userIDS) {
        Collection<Participant> participants =
                userIDS
                        .stream()
                        .map(userID -> new Participant(userID, true))
                        .collect(Collectors.toList());
        return new Conversation(UUID.randomUUID(), participants, true);
    }

    public boolean isActive() {
        return active;
    }

    public UUID getID() {
        return id;
    }

    public boolean isUserParticipant(UserID userID) {
        return participants
                .stream()
                .anyMatch(p -> p.isUser(userID) && p.isInConversation());
    }

    public void leave(UserID userID) {
        Participant participant = getActiveParticipantOrThrow(userID);
        participant.leave();

        long numActive = participants.stream()
                .filter(Participant::isInConversation)
                .count();

        if (numActive < 2) {
            active = false;
        }
    }

    private Participant getActiveParticipantOrThrow(UserID userID) {
        Participant participant = getParticipantOrThrow(userID);
        if (!participant.isInConversation())
            throw new IllegalStateException(String.format("Participant %s is not active in conversation %s", userID, id));
        return participant;
    }

    private Participant getParticipantOrThrow(UserID userID) {
        return participants
                .stream()
                .filter(p -> p.isUser(userID))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format("User %s is not part of conversation %s", userID, id)));
    }

    public void expire() {
        active = false;
    }
}
