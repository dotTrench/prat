package com.stanken.application.conversation;

import com.stanken.application.conversation.commands.SendMessageCommand;
import com.stanken.application.conversation.exceptions.ConversationNotActiveException;
import com.stanken.application.conversation.exceptions.ConversationNotFoundException;
import com.stanken.application.conversation.exceptions.UserNotPartOfConversationException;
import com.stanken.domain.conversation.*;

import java.util.UUID;

/**
 * Created by erik on 2017-06-19.
 */
public class MessageService {
    private final ConversationRepository conversationRepository;
    private final MessageRepository messageRepository;

    public MessageService(ConversationRepository conversationRepository, MessageRepository messageRepository) {
        this.conversationRepository = conversationRepository;
        this.messageRepository = messageRepository;
    }

    public void sendMessage(SendMessageCommand command) {
        Conversation conversation = conversationRepository.findByID(command.conversationID)
                .orElseThrow(() -> new ConversationNotFoundException(command.conversationID));

        UserID userID = UserID.from(command.sender);
        if (!conversation.isUserParticipant(userID))
            throw new UserNotPartOfConversationException(command.conversationID, command.sender);

        if (!conversation.isActive())
            throw new ConversationNotActiveException(command.conversationID);

        messageRepository.save(new Message(UUID.randomUUID(), command.conversationID, command.message, UserID.from(command.sender)));
    }
}
