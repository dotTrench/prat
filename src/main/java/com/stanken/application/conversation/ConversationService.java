package com.stanken.application.conversation;

import com.stanken.application.conversation.commands.LeaveConversationCommand;
import com.stanken.application.conversation.commands.StartConversationCommand;
import com.stanken.application.conversation.exceptions.ConversationNotFoundException;
import com.stanken.application.conversation.exceptions.NotEnoughParticipantsException;
import com.stanken.application.conversation.exceptions.UserNotPartOfConversationException;
import com.stanken.domain.conversation.Conversation;
import com.stanken.domain.conversation.ConversationRepository;
import com.stanken.domain.conversation.UserID;

import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Created by erik on 2017-06-15.
 */
public class ConversationService {
    private final ConversationRepository conversationRepository;

    public ConversationService(ConversationRepository conversationRepository) {
        this.conversationRepository = conversationRepository;
    }

    public void startConversation(StartConversationCommand command) {
        if (command.participants.size() < 2) throw new NotEnoughParticipantsException(command.participants);
        Collection<UserID> participants =
                command.participants
                        .stream()
                        .map(UserID::from)
                        .collect(Collectors.toList());

        Conversation.start(participants);
        Conversation conversation = Conversation.start(participants);

        conversationRepository.save(conversation);
    }

    public void leaveConversation(LeaveConversationCommand command) {
        Conversation conversation = conversationRepository.findByID(command.conversationID)
                .orElseThrow(() -> new ConversationNotFoundException(command.conversationID));

        UserID userID = UserID.from(command.userID);
        if (!conversation.isUserParticipant(userID)) {
            throw new UserNotPartOfConversationException(conversation.getID(), command.userID);
        }

        conversation.leave(userID);

        conversationRepository.save(conversation);
    }
}
