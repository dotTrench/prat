package com.stanken.application.shared;

/**
 * Created by erik on 2017-06-19.
 */
public class CommandException extends RuntimeException {
    public CommandException(String message) {
        super(message);
    }
}
