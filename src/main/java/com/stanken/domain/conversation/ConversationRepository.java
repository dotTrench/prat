package com.stanken.domain.conversation;

import java.util.Optional;
import java.util.UUID;

/**
 * Created by erik on 2017-06-15.
 */
public interface ConversationRepository {
    void save(Conversation conversation);

    Optional<Conversation> findByID(UUID id);
}
