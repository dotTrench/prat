package com.stanken.application.conversation.commands;

import java.util.UUID;

/**
 * Created by erik on 2017-06-19.
 */
public class SendMessageCommand {
    public final UUID conversationID;
    public final String message;
    public final UUID sender;

    public SendMessageCommand(UUID conversationID, String message, UUID sender) {
        this.conversationID = conversationID;
        this.message = message;
        this.sender = sender;
    }
}
