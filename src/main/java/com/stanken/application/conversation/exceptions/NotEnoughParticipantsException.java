package com.stanken.application.conversation.exceptions;

import com.stanken.application.shared.CommandException;

import java.util.Collection;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Created by erik on 2017-06-15.
 */
public class NotEnoughParticipantsException extends CommandException {
    public NotEnoughParticipantsException(Collection<UUID> participants) {

        super(String.format("Needs at least 2 participants to start conversation got %d [%s]",
                participants.size(),
                participants.stream().map(UUID::toString).collect(Collectors.joining(","))));
    }
}
