package com.stanken.application.conversation.exceptions;

import com.stanken.application.shared.AggregateNotFoundException;

import java.util.UUID;

/**
 * Created by erik on 2017-06-19.
 */
public class ConversationNotFoundException extends AggregateNotFoundException {
    public ConversationNotFoundException(UUID conversationID) {
        super(String.format("Could not find conversation with ID %s", conversationID));
    }
}
