package com.stanken.domain.conversation;

/**
 * Created by erik on 2017-06-15.
 */
public class Participant {
    private final UserID userID;
    private boolean inConversation;

    public Participant(UserID userID, boolean inConversation) {
        this.userID = userID;
        this.inConversation = inConversation;
    }

    public boolean isUser(UserID userID) {
        return this.userID.equals(userID);
    }

    public boolean isInConversation() {
        return inConversation;
    }

    public void leave() {
        inConversation = false;
    }
}
