package com.stanken.application.shared;

/**
 * Created by erik on 2017-06-19.
 */
public class AggregateNotFoundException extends CommandException {
    public AggregateNotFoundException(String message) {
        super(message);
    }
}
