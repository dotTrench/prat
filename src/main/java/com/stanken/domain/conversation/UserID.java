package com.stanken.domain.conversation;

import java.util.UUID;

/**
 * Created by erik on 2017-06-15.
 */
public final class UserID {
    private final UUID id;

    private UserID(UUID id) {
        if (id == null) throw new IllegalArgumentException("id cant be null");
        this.id = id;
    }

    public UserID() {
        id = UUID.randomUUID();
    }

    public static UserID from(UUID id) {
        return new UserID(id);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof UserID && ((UserID) obj).id.equals(this.id);
    }

    public UUID asUUID() {
        return id;
    }
}
